//
//  UIColor+AppTheme.swift
//  askfriends
//
//  Created by Sonya Olson on 1/7/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

extension UIColor{
    public class var appDarkGray: UIColor
    {
        return UIColor(red: 31.0/255.0, green: 30.0/255.0, blue: 42.0/255.0, alpha: 1.0)
    }
    
    public class var appGray: UIColor
    {
        return UIColor(red: 107.0/255.0, green: 103.0/255.0, blue: 138.0/255.0, alpha: 1.0)
    }
    
    public class var appLightGray: UIColor
    {
        return UIColor(red: 212.0/255.0, green: 210.0/255.0, blue: 222.0/255.0, alpha: 1.0)
    }
    
    public class var appPink: UIColor{
        return UIColor(red: 255.0/255.0, green: 61.0/255.0, blue: 90.0/255.0, alpha: 1.0)
    }
}

