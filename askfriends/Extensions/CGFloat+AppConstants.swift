//
//  NSLayoutConstraint+Constants.swift
//  askfriends
//
//  Created by Sonya Olson on 1/7/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

extension CGFloat {
    public static let appConstant = CGFloat(12.0)
    public static let appHalfConstant = CGFloat(6.0)
    public static let appDoubleConstant = CGFloat(24.0)
}
