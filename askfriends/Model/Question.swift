//
//  Question.swift
//  askfriends
//
//  Created by Sonya Olson on 1/25/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

@objc protocol Question {
    var id          :String?    {get set}
    var title       :String?    {get set}
    var numComments :Int32      {get set}
    var link        :String?    {get set}
    var saved       :Bool       {get set}
    var source      :String?    {get set}
}

