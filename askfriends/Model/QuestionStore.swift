//
//  QuestionStore.swift
//  askfriends
//
//  Created by Sonya Olson on 5/12/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

protocol QuestionStore: class {
    var delegate:SavedQuestionListDelegate? { get set}
    
    var count:Int { get }
    
    func questionAtIndex(_ index:Int) -> Question?
    func insertQuestion(_ question:Question)
    func removeQuestion(_ question:Question)
    func removeQuestionAtIndex(_ index:Int)
    func questionById(_ id:String) -> Question?
    
    //func save(_ question:Question)
}
