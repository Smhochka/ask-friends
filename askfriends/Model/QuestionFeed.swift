//
//  Feed.swift
//  askfriends
//
//  Created by Sonya Olson on 1/17/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
protocol QuestionFeed: class{
    
    func next(_ completionHandler: @escaping (_ item:Question?, _ success:Bool) -> Void)
    func previous() -> Question?
    
}

