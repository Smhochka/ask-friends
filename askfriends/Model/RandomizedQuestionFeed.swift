//
//  RandomizedQuestionFeed.swift
//  askfriends
//
//  Created by Sonya Olson on 7/7/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

class RandomizedQuestionFeed: NSObject {
    var source:QuestionStore
    var indexList:Array<Int>
    var currentLocation: Int
    
    init(withSource source: QuestionStore){
        
        self.source = source
        self.currentLocation = 0
        
        guard (self.source.count != 0) else {
            self.indexList = []
            return
        }
        
        guard (self.source.count != 1) else {
            self.indexList = [0]
            return
        }
        
        let limit = min(self.source.count-1, 100)
        self.indexList = Array(0...limit).shuffled()
    }
    
    func nextLocation() -> Int {
        if(self.currentLocation == self.indexList.count-1){
            return 0
        }else{
            return self.currentLocation + 1
        }
    }
    
    func previousLocation() -> Int {
        if(self.currentLocation == 0){
            return self.indexList.count - 1
        }else{
            return self.currentLocation - 1
        }
    }
    
    func currentQuestion() -> Question {
        let index = self.indexList[self.currentLocation]
        return self.source.questionAtIndex(index)!
    }
}

extension RandomizedQuestionFeed: QuestionFeed{
    func next(_ completionHandler: @escaping (Question?, Bool) -> Void) {
        completionHandler(self.currentQuestion(), true)
        self.currentLocation = self.nextLocation()
    }
    
    func previous() -> Question? {
        let question = self.currentQuestion()
        self.currentLocation = self.previousLocation()
        return question
    }
}

extension MutableCollection {
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: Int = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}
