//
//  ASKQuestionView.swift
//  
//
//  Created by Sonya Olson on 4/18/16.
//
//

import UIKit

@available(iOS 11.0, *)
class QuestionView: UIView{

    var nextButton:UIButton = UIButton()
    var questionView = UIView();
    
    var questionLabel:UILabel = UILabel()
    var commentView:UIView = UIView()
    
    var commentButton:UIButton = UIButton()
    var commentIcon:UIImageView = UIImageView()
    
    var favoriteButton:UIButton = UIButton()
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        
        setup()
    }
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(withQuestion question:Question){
        self.questionLabel.text = question.title
        self.commentButton.setTitle(String(format:"%i comments", question.numComments), for: UIControlState())
        
        let favoriteButtonText = question.saved ? "★" : "☆"
        self.favoriteButton.setTitle(favoriteButtonText, for: UIControlState.normal)
        self.favoriteButton.isHidden = false
        
        self.stopLoadingIndicator()
    }
    
    private func setup(){
        self.backgroundColor = UIColor.appDarkGray;
        
        self.activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        self.activityIndicator.isHidden = false;
        self.questionView.addSubview(activityIndicator)
        
        self.questionView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(questionView)
        
        self.nextButton.setTitleColor(UIColor.white, for: UIControlState())
        self.nextButton.layer.backgroundColor = (UIColor.appPink).cgColor
        self.nextButton.setTitleColor(UIColor.appGray, for: UIControlState.highlighted)
        self.nextButton.setTitle("Next", for: UIControlState())
        self.nextButton.titleLabel?.textAlignment = NSTextAlignment.center
        self.nextButton.titleLabel?.font = UIFont.systemFont(ofSize: 30, weight: UIFont.Weight.light)
        self.nextButton.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(nextButton)
        
        self.favoriteButton.setTitle("⭐︎", for: UIControlState.highlighted)
        self.favoriteButton.setTitle("☆", for: UIControlState.normal)
        self.favoriteButton.titleLabel?.textAlignment = NSTextAlignment.center
        self.favoriteButton.titleLabel?.font = UIFont.systemFont(ofSize: 26, weight: UIFont.Weight.medium)
        self.favoriteButton.translatesAutoresizingMaskIntoConstraints = false
        self.questionView.addSubview(self.favoriteButton)
        
        self.questionLabel.textColor = UIColor.white
        self.questionLabel.text = ""
        self.questionLabel.numberOfLines = 0
        self.questionLabel.textAlignment = NSTextAlignment.center
        self.questionLabel.font = UIFont.systemFont(ofSize: 30, weight: UIFont.Weight.light)
        self.questionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.questionView.addSubview(questionLabel)
        
        self.commentView.translatesAutoresizingMaskIntoConstraints = false
        self.questionView.addSubview(commentView)
        
        self.commentButton.setTitleColor(UIColor.appGray, for: UIControlState())
        self.commentButton.setTitle("Starting Size", for: UIControlState())
        self.commentButton.titleLabel?.textAlignment = NSTextAlignment.center
        self.commentButton.titleLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.medium)
        self.commentButton.translatesAutoresizingMaskIntoConstraints = false
        self.commentView.addSubview(commentButton)
        
        self.commentIcon.image = UIImage(named: "comment-16")
        self.commentIcon.translatesAutoresizingMaskIntoConstraints = false;
        self.commentView.addSubview(commentIcon)
        
        self.setupConstraints()
        
        self.startLoadingIndicator()
        
    }
    
    private func setupConstraints(){
        questionView.topAnchor.constraint(equalTo:self.topAnchor , constant: 0).isActive = true
        questionView.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor, constant: 0).isActive = true
        questionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -100).isActive = true
        questionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        
        nextButton.topAnchor.constraint(equalTo: self.bottomAnchor, constant: -100).isActive = true
        nextButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        nextButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        nextButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        
        questionLabel.centerYAnchor.constraint(equalTo: questionView.centerYAnchor, constant: 0).isActive = true
        questionLabel.centerXAnchor.constraint(equalTo: questionView.centerXAnchor, constant: 0).isActive = true
        questionLabel.leadingAnchor.constraint(equalTo: questionView.leadingAnchor, constant: .appConstant).isActive = true
        questionLabel.trailingAnchor.constraint(equalTo: questionView.trailingAnchor, constant: -.appConstant).isActive = true
        
        favoriteButton.topAnchor.constraint(equalTo: self.layoutMarginsGuide.topAnchor, constant: .appConstant).isActive = true
        favoriteButton.trailingAnchor.constraint(equalTo: questionView.trailingAnchor, constant: -.appConstant).isActive = true
        favoriteButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        favoriteButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        commentIcon.centerYAnchor.constraint(equalTo: commentView.centerYAnchor, constant: 0).isActive = true
        commentIcon.leadingAnchor.constraint(equalTo: commentView.leadingAnchor, constant: 0).isActive = true
        commentIcon.heightAnchor.constraint(equalToConstant: 16).isActive = true
        commentIcon.widthAnchor.constraint(equalTo: commentIcon.heightAnchor).isActive = true
        
        commentButton.topAnchor.constraint(equalTo: commentView.topAnchor).isActive = true
        commentButton.heightAnchor.constraint(equalToConstant: 46).isActive = true
        commentButton.leadingAnchor.constraint(equalTo: commentIcon.trailingAnchor, constant: .appConstant).isActive = true
        commentButton.trailingAnchor.constraint(equalTo:commentView.trailingAnchor).isActive = true
        
        commentView.topAnchor.constraint(equalTo: questionLabel.bottomAnchor, constant: .appConstant).isActive = true
        commentView.heightAnchor.constraint(equalToConstant: 46).isActive = true
        commentView.widthAnchor.constraint(equalTo: questionView.widthAnchor, multiplier: 1)
        commentView.centerXAnchor.constraint(equalTo:questionView.centerXAnchor).isActive = true
        
        activityIndicator.centerXAnchor.constraint(equalTo:self.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo:self.centerYAnchor, constant:-100).isActive = true
        
    }
    
    func startLoadingIndicator(){
        self.activityIndicator.startAnimating()
        
        self.questionLabel.isHidden = true
        self.commentButton.isHidden = true
        self.commentIcon.isHidden = true
        
        self.nextButton.isEnabled = false
        self.nextButton.setTitle("Next", for: UIControlState())
    }
    
    func stopLoadingIndicator(){
        self.activityIndicator.stopAnimating()
        
        self.questionLabel.isHidden = false
        self.commentButton.isHidden = false
        self.commentIcon.isHidden = false
        
        self.nextButton.isEnabled = true
        self.nextButton.setTitle("Next", for: UIControlState())
    }
    
    func displayConnectionError(){
        self.activityIndicator.stopAnimating()
        
        self.questionLabel.isHidden = false
        self.commentButton.isHidden = true
        self.commentIcon.isHidden = true
        
        self.questionLabel.text = "How would your life be different without the internet?"
        
        self.nextButton.isEnabled = true
        self.nextButton.setTitle("Try Again", for: UIControlState())
        
        self.favoriteButton.isHidden = true
    }
}

