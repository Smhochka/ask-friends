//
//  QuestionCellTableViewCell.swift
//  askfriends
//
//  Created by Sonya Olson on 1/7/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

class QuestionCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    let title: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.appLightGray
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        return label
    }()
    
    func setup(with question:Question){
        self.title.text = question.title
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        addSubview(title)
        
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        title.translatesAutoresizingMaskIntoConstraints = false
        title.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1 * .appDoubleConstant).isActive = true
        title.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: .appConstant).isActive = true
        title.topAnchor.constraint(equalTo: self.topAnchor, constant: .appDoubleConstant).isActive = true
        title.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -1 * .appConstant).isActive = true
        
    }
}
