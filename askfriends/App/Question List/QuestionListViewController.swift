//
//  QuestionListTableView.swift
//  askfriends
//
//  Created by Sonya Olson on 1/7/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit
import SafariServices

@available(iOS 11.0, *)
class QuestionListViewController: UIViewController {
    private let tableView = UITableView()
    private var list:SavedQuestionList
    
    // Question View
    let questionViewController = UIViewController()
    let questionView = QuestionView()
    
    init(withList list:SavedQuestionList){
        self.list = list
        super.init(nibName: nil, bundle: nil)
        
        self.list.load()
        self.questionViewController.view = questionView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // TODO: Prevent this from being refreshed every time
        self.list.load()
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Styles
        tableView.backgroundColor = .appDarkGray
        tableView.separatorColor = .appGray
        
        // Table View
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(QuestionCell.self, forCellReuseIdentifier: "questionCell")
        
        // Layout Constraints
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        self.title = "★ Favorites"
    }
}

@available(iOS 11.0, *)
extension QuestionListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell", for: indexPath)
        if let questionCell = cell as? QuestionCell {
            questionCell.setup(with:list.questionAtIndex(indexPath.row)!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let question = list.questionAtIndex(indexPath.row),
            let link = question.link
        {
            
            // Present Safari View
            let vc = RedditViewControllers.SafariViewControllerFromLink(link)
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            // Remove the item from the data model
            list.removeQuestionAtIndex(indexPath.row)
            
            // Delete the table view row
            tableView.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
}


