//
//  SavedQuestionsNavigationController.swift
//  askfriends
//
//  Created by Sonya Olson on 1/7/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class SavedQuestionViewController: UINavigationController {
    var controllerDelegate: SavedQuestionViewControllerDelegate?
    
    private var list: SavedQuestionList?
    private var tableVC:QuestionListViewController
    private var doneButton = {
        return UIBarButtonItem(title: "Done", style: .plain, target: self, action:  #selector(doneButtonPressed(_:)))
    }()
    private var startButton = {
        return UIBarButtonItem(title: "Play", style: .plain, target: nil, action:  #selector(startButtonPressed(_:)))
    }()

    init(withList list:SavedQuestionList){
        self.list = list
        self.tableVC = QuestionListViewController(withList:list)
        super.init(nibName: nil, bundle: nil)
        
        self.viewControllers.append(tableVC)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // View
        self.view.backgroundColor = .appDarkGray
        
        // Navigation Bar
        self.navigationBar.barStyle = .blackTranslucent
        self.navigationBar.prefersLargeTitles = true
        
        // Done Button
        self.doneButton.target = self
        self.tableVC.navigationItem.leftBarButtonItem = self.doneButton
        
        // Start Button
        self.startButton.target = self
        self.tableVC.navigationItem.rightBarButtonItem = self.startButton
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func doneButtonPressed(_ sender:UIBarButtonItem){
        guard sender == self.doneButton else {return}
        
        if let delegate = self.controllerDelegate {
            delegate.savedQuestionViewControllerDidFinish(self, newFeed: nil)
        }
    }
    
    @objc private func startButtonPressed(_ sender:UIBarButtonItem){
        guard sender == self.startButton else {return}

        if let list = self.list,
           list.count > 0,
           let delegate = self.controllerDelegate
        {
            delegate.savedQuestionViewControllerDidFinish(self, newFeed: RandomizedQuestionFeed(withSource: list))
        }
    }
}

protocol SavedQuestionViewControllerDelegate{
    func savedQuestionViewControllerDidFinish(_ vc:SavedQuestionViewController, newFeed: QuestionFeed?)
}


