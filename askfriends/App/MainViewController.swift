//
//  AppNavigationController.swift
//  askfriends
//
//  Created by Sonya Olson on 1/9/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit
import StoreKit

class MainViewController: UIViewController {
    var navController:UINavigationController
    
    // Constants
    let listControllerContent = SavedQuestionList()
    let gameControllerContent:QuestionFeed = RedditFeed()
    
    // Presentation (Game) Mode
    var gameController:UIViewController
    
    // Saved Questions
    var listController:SavedQuestionViewController
    var viewSavedListButton:UIBarButtonItem?
    var endSavedListGameButton: UIBarButtonItem?
    var savedListGameController:UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.applyStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    init() {
        // Init View Controllers
        self.gameController = QuestionGameController(withFeed:self.gameControllerContent, store:self.listControllerContent as QuestionStore)
        self.listController = SavedQuestionViewController(withList:self.listControllerContent)
        self.navController = UINavigationController(rootViewController: self.gameController)
        
        // Init Self
        super.init(nibName: nil, bundle: nil)
        
        // Button Setup
        self.viewSavedListButton = UIBarButtonItem.init(
            title: "Favorites",
            style: .plain,
            target: self,
            action: #selector(viewFavorites(sender:))
        )
        
        // Button Setup
        self.endSavedListGameButton = UIBarButtonItem.init(
            title: "End",
            style: .plain,
            target: self,
            action: #selector(popSavedListGameController)
        )
        
        setupWithDefaultFeed()
    
    }
    
    func setupWithDefaultFeed() {
        
        // Remove Saved List Game View
        if let savedListGameController = self.savedListGameController {
            savedListGameController.view.removeFromSuperview()
            savedListGameController.removeFromParentViewController()
        }
        
        // Nav Controller Setup
        self.view.addSubview(navController.view)
        self.addChildViewController(navController)
        self.listController.controllerDelegate = self
        
        self.gameController.navigationItem.leftBarButtonItem = self.viewSavedListButton
    }
    
    func pushSavedQuestionFeed() {
        if let savedListGameController = self.savedListGameController {
            self.navController.pushViewController(self.savedListGameController!, animated: false)
            savedListGameController.navigationItem.setHidesBackButton(true, animated: false)
            savedListGameController.navigationItem.leftBarButtonItem = self.endSavedListGameButton
            savedListGameController.title = "★ Favorites"
            self.navController.setNavigationBarHidden(true, animated: true)
        }
    }
    
    @objc func popSavedListGameController (){
        self.promptForReview()
        self.navController.popViewController(animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func viewFavorites(sender: UIBarButtonItem){
        self.navController.present(self.listController, animated: true, completion: nil)
    }
}

extension MainViewController:SavedQuestionViewControllerDelegate{
    func savedQuestionViewControllerDidFinish(_ vc: SavedQuestionViewController, newFeed: QuestionFeed?) {
        self.dismiss(animated: true, completion: nil)
        
        if let feed = newFeed {
            self.savedListGameController = QuestionGameController(withFeed:feed, store:self.listControllerContent as QuestionStore)
            pushSavedQuestionFeed()
        }
        
    }
}

// MARK: Private Helpers
private extension MainViewController{
    
    func applyStyle(){
        
        // Self
        self.view.backgroundColor = .appDarkGray
        
        // Nav Controller
        self.navController.navigationBar.barStyle = .blackTranslucent
        self.navController.navigationBar.prefersLargeTitles = true
        
    }
    
    func promptForReview(){
        let count = UserDefaults.standard.integer(forKey: "completedFavoriteGameCount")
        
        if(count == 2){
            SKStoreReviewController.requestReview()
        }
        
        UserDefaults.standard.set(count + 1, forKey: "completedFavoriteGameCount")
        
    }
}
