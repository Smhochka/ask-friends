//
//  ViewController.swift
//  askfriends
//
//  Created by Sonya Olson on 4/18/16.
//  Copyright (c) 2016 Sonya Olson. All rights reserved.
//

import UIKit
import SafariServices

@available(iOS 11.0, *)
class QuestionGameController: UIViewController {
    
    var questionFeed:QuestionFeed
    var questionStore:QuestionStore
    var enableFavoriteToggle = true
    
    private let questionView = QuestionView.init()
    private var link:String?
    private var question:Question?
    
    init(withFeed feed: QuestionFeed, store: QuestionStore){
        self.questionFeed = feed
        self.questionStore = store
        
        super.init(nibName:nil,bundle:nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc override func viewDidLoad() {
        super.viewDidLoad()

        self.view = questionView
        
        self.bindActions()
        self.loadQuestion()
        
        // Navigation Controller Config
        if let navController = self.navigationController{
            navController.navigationBar.prefersLargeTitles = false
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateView()
        self.setNavigationBarHidden(false) // Start with the Navbar Showing
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent!) {
        if(event.subtype == UIEventSubtype.motionShake) {
            if (self.presentedViewController == nil){
                if let question = questionFeed.previous() {
                    self.link = question.link
                    self.questionView.setup(withQuestion: question)
                    self.question = question
                }
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.setNavigationBarHidden(nil)
    }
    
}

// MARK: UI Event Selectors
private extension QuestionGameController{
    
    func bindActions(){
        self.questionView.commentButton.addTarget(self, action:#selector(QuestionGameController.openURL), for: UIControlEvents.touchUpInside)
        self.questionView.nextButton.addTarget(self, action:#selector(QuestionGameController.loadQuestion), for: UIControlEvents.touchUpInside)
        self.questionView.favoriteButton.addTarget(self, action:#selector(QuestionGameController.toggleFavorite), for: UIControlEvents.touchUpInside)
    }
    
    @objc func loadQuestion(){
    
        // Update View
        self.questionView.startLoadingIndicator()
        self.setNavigationBarHidden(true)
        
        _ = self.questionFeed.next({ (question,success) -> Void in
            
            guard success else {
                
                // Show connection error screen
                DispatchQueue.main.async(execute: {
                    self.questionView.displayConnectionError()
                })
                
                return
            }
            
            // On the main thread, update the UI
            DispatchQueue.main.async(execute: {
                
                self.question = question!
                self.link = question!.link
                
                self.question?.saved = self.questionStore.questionById(question!.id!) != nil
                
                self.updateView()
                
                self.questionView.stopLoadingIndicator()
                
            })
            
        })
    }
    
    @objc func openURL() {
        let vc = RedditViewControllers.SafariViewControllerFromLink(link!)
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
}

// MARK: Private Helpers
private extension QuestionGameController{
    
    func updateView(){
        if let question = self.question{
            self.questionView.setup(withQuestion: question)
        }
    }
    
    @objc func toggleFavorite(){
        
        if let question = self.question {
            //Toggle Save Value
            switch(question.saved == true){
                
                case true:
                    self.questionStore.removeQuestion(question)
                    self.question!.saved = false
                    break
                
                case false:
                    self.questionStore.insertQuestion(question)
                    self.question!.saved = true
                    break
                
            }
            
            // Update the question
            self.updateView()
        }
    }
    
    func setNavigationBarHidden(_ preferredState:Bool?){
        guard let navController = self.navigationController else { return }
        
        let hidden = preferredState != nil ? preferredState : navController.navigationBar.isHidden != true
        
        navController.setNavigationBarHidden(hidden!, animated: true)
        
    }
}

extension QuestionGameController:SFSafariViewControllerDelegate,UINavigationControllerDelegate{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}
