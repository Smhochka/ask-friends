//
//  RedditSafariViewController.swift
//  askfriends
//
//  Created by Sonya Olson on 1/9/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit
import SafariServices

class RedditViewControllers: NSObject {
    
    static func SafariViewControllerFromLink(_ link:String) -> SFSafariViewController{
        
        // URL
        var url = URL(string:"https://www.reddit.com") // Default url
        
        if let linkAsURL = URL(string: link) {
            url = linkAsURL
        }
    
        // Configuration
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true
        
        // Create VC
        let vc = SFSafariViewController(url: url!, configuration: config)
        vc.preferredControlTintColor = .appPink
    
        return vc
    }
    
}

