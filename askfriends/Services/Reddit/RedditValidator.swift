//
//  LanguageFilter.swift
//  askfriends
//
//  Created by Sonya Olson on 5/12/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

class RedditValidator: NSObject {
    
    static func isValid(_ post:RedditPost) -> Bool {
        
        if let title = post.title {
            let containsRejectedTerm = self.containsRejectedTerm(title)
            
            return !containsRejectedTerm
                && !post.stickied
                && post.seen == nil
                && !post.over18
        } else {
            return false
        }
        
    }
}

//MARK: Static Functions
extension RedditValidator {
    
    static func cleanTitle(_ title:String) -> String{
        
        var cleanTitle = title
        
        cleanTitle = cleanTitle.replacingOccurrences(of: "[Serious]", with: "")
        cleanTitle = cleanTitle.replacingOccurrences(of: "[serious]", with: "")
        cleanTitle = cleanTitle.replacingOccurrences(of: "[SERIOUS]", with: "")
        cleanTitle = cleanTitle.replacingOccurrences(of: "(Serious)", with: "")
        cleanTitle = cleanTitle.replacingOccurrences(of: "(serious)", with: "")
        
        cleanTitle = cleanTitle.replacingOccurrences(of: "&amp;", with: "&")
        
        return cleanTitle
    }
    
    static func containsRejectedTerm(_ phrase:String) -> Bool {
        let subject = phrase.lowercased()
    
        return subject.contains("reddit")
        || subject.contains("fuck")
        || subject.contains("pussy")
        || subject.contains("shit")
    }
}
