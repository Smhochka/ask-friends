//
//  Post.swift
//  askfriends
//
//  Created by Sonya Olson on 1/16/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

class RedditPost: Question {
    var id:String?
    
    var title:String?
    var numComments: Int32
    var link:String?
    
    var stickied:Bool
    var seen:Bool?
    var over18:Bool
    var rejected:Bool
    
    var source:String?
    
    var saved:Bool
    
    init(withTitle title:String,
         _            id:String,
         _   numComments:Int,
         _          link:String){
        
        self.title = RedditValidator.cleanTitle(title)
        self.id = id
        self.numComments = Int32(numComments)
        self.link = link
        self.source = "reddit"
        
        stickied = false
        over18 = false
        rejected = false
        saved = false
        
    }
    
    

    
}

