//
//  ASKReddit.swift
//
//  Created by Sonya Olson on 4/19/16.
//
//

import UIKit
import CoreData

@available(iOS 11.0, *)
class RedditFeed: NSObject {    
    private var subreddit: String = ""

    private static let strict = true    // Strict mode filters out more questions

    private var feed    :[RedditPost] = []    // currently loaded posts
    private var seen    :[String] = []  // all seen during this session
    private var count   = 0             // current position in the feed
    private var lastId  = ""            // id of the last loaded post
    
    // most recent questions accessed
    private var currentPost:RedditPost?
    private var previousPost:RedditPost?
    
    convenience init(withSubreddit subreddit:String?){
        self.init()
        self.subreddit = subreddit ?? ""
    }
    
    private func loadRedditJSON(_ count:Int?, lastId:String?, completionHandler: @escaping (_ success:Bool) -> Void){

        let url = RedditService.urlForSubreddit("askreddit", afterId: lastId)
        
        RedditService.fetch(fromURL: url) { (success, json) in
            guard success else { completionHandler(false); return}

            if let results = RedditParser.parseList(json!){
                
                self.updateFeedWithPosts(results)
                completionHandler(true)
            
            }else{
                completionHandler(false)
            }
        }
    
    }
    
    private func updateFeedWithPosts(_ posts:[RedditPost]){
        for post in posts{
            if(RedditValidator.isValid(post)){
                self.addPost(post)
            }
        }
    }
    
    private func addPost(_ post:RedditPost){
        guard (post.id != nil) else { return }
        
        self.feed.append(post)
        self.seen.append(post.id!)
        self.lastId = post.id!
    }
}

extension RedditFeed: QuestionFeed {
    
    func next(_ completionHandler: @escaping (_ post:Question?, _ success:Bool) -> Void) {
        
        if(self.feed.count == 0){
            
            // load more posts
            self.count = self.count + 25
            self.loadRedditJSON( count, lastId: self.lastId, completionHandler: { (_ success:Bool) -> Void in
                
                // return immediately on failure
                if(success == false){
                    completionHandler(nil, false)
                    return
                }
                
                // dequeue a post
                if let nextPost = self.feed.remove(at: 0) as RedditPost?{
                    
                    // update the post history
                    self.previousPost = self.currentPost
                    self.currentPost = nextPost
                    
                    // callback
                    completionHandler(nextPost, true)
                }
            })
            
        }else{
            
            // dequeue a question
            if let nextPost = self.feed.remove(at: 0) as RedditPost? {
            
                // update the question history
                self.previousPost = self.currentPost
                self.currentPost = nextPost
                
                // callback
                completionHandler(nextPost, true)
            }
            
        }
    }
    
    func previous() -> Question?{
        
        // add the current post back to the queue
        if let current = self.currentPost {
            self.feed.insert(current, at: 0)
            
            // revert question history
            self.currentPost = previousPost
            self.previousPost = nil
        }
        
        return self.currentPost
    }
    
    
}

