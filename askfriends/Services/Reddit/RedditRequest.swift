//
//  Request.swift
//  askfriends
//
//  Created by Sonya Olson on 1/16/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

class RedditService: NSObject {
    static let session = URLSession.shared
    static let endpoint = "https://www.reddit.com/"
    static let batchSize = 25
    
    static func fetch(fromURL url:URL, _ completion: @escaping (_ success:Bool, _ data:Any?) -> Void){
        
        let networkTask = session.dataTask(with: url, completionHandler : { data, response, error -> Void in
            guard error == nil else { completion(false,nil); return }
            
            do{
                
                // Parse JSON
                let json = try JSONSerialization.jsonObject(with:data!, options:JSONSerialization.ReadingOptions.mutableContainers)
                completion(true, json)
                return
                    
            }catch{
        
                completion(false, nil)
                return
                
            }

        })
        
        networkTask.resume()
    }
    
    static func urlForSubreddit(_ subreddit:String, afterId:String?) -> URL{
        var url = RedditService.endpoint + "r/"+subreddit+".json"
        
        if let id = afterId {
            url = String(format: "%@?count=%i&after=t3_%@", url, RedditService.batchSize, id)
        }
        
        return URL(string:url)!
    }
}
