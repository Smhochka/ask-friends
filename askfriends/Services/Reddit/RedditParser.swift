//
//  RedditResponse.swift
//  askfriends
//
//  Created by Sonya Olson on 1/16/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit

class RedditParser: NSObject {
    typealias ListResponse = [String: [String: Any]? ]
    
    static func parseList(_ json:Any) -> [RedditPost]?{
        var posts:[RedditPost] = []
        
        guard let response = json as? [String:Any],
            let data = response["data"] as? [String:Any],
            let list = data["children"] as? [Any]
            else {
                return nil
        }
        
        for item in list{
            posts.append(self.parsePost(item)!)
        }

        return posts
    }
    
    static func parsePost(_ data:Any) -> RedditPost?{
        
        guard let dataAsDict = data as? Dictionary<String,Any>,
            let postData = dataAsDict["data"] as? Dictionary<String,Any>
    
            else {
                return nil
        }
        
        let id             = postData["id"]            as? String  ?? ""
        let title          = postData["title"]         as? String  ?? ""
        let numComments    = postData["num_comments"]  as? Int     ?? 0
        let link           = postData["url"]           as? String  ?? ""
        let stickied       = postData["stickied"]      as? Bool    ?? false
        let over18         = postData["over_18"]       as? Bool    ?? false
        
        let post = RedditPost.init(withTitle: title, id, numComments, link)
        
        post.over18 = over18
        post.stickied = stickied
        
        return post
        
    }
    
}
