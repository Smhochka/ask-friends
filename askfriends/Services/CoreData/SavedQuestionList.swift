//
//  QuestionList.swift
//  askfriends
//
//  Created by Sonya Olson on 1/7/18.
//  Copyright © 2018 Sonya Olson. All rights reserved.
//

import UIKit
import CoreData
import os

protocol SavedQuestionListDelegate{
    func savedQuestionListDidChange(_ list:SavedQuestionList)
}

@available(iOS 11.0, *)
class SavedQuestionList: NSObject, QuestionStore {
    
    var delegate:SavedQuestionListDelegate?
    
    private var list:[SavedQuestion]? = []

    var count:Int{
        return list!.count
    }
    
    func questionAtIndex(_ index:Int)->Question?{
        guard index < self.list!.count else { return nil }
        return list![index] as Question
    }
    
    func removeQuestionAtIndex(_ index:Int){
        guard index < self.list!.count else { return }
        let q = self.list?.remove(at: index)
        
        do{
            try SavedQuestion.delete(q!)
            self.listChanged()
        } catch let error as NSError {
            os_log("Could not delete. %@", error)
        }
    }
    
    func removeQuestion(_ question:Question){
        do {
            try SavedQuestion.deleteEntityFromQuestion(question)
        } catch let error as NSError {
            os_log("Could not delete. %@", error)
        }
        
        self.listChanged()
    }
    
    func insertQuestion(_ question: Question) {
        do {
            let _ = try SavedQuestion.insertQuestion(question);
        } catch let error as NSError {
            os_log("Could not create. %@", error)
        }
        
        self.listChanged();
    }
    
    func questionById(_ id:String) -> Question? {
        
        // Create Request
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "SavedQuestion")
        fetchRequest.predicate = NSPredicate(format: "id = %@", id as CVarArg)
        
        // Execute Fetch
        do {
            let match = try SavedQuestion.context.fetch(fetchRequest) as? [SavedQuestion]
            return match?.first
        } catch let error as NSError {
            os_log("Could not fetch. %@", error)
            return nil
        }
    }
    
    private func listChanged(){
        guard let delegate = self.delegate else {return}
        
        delegate.savedQuestionListDidChange(self)
    }

}

extension SavedQuestionList{
    
    func load(){

        // Create Request
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SavedQuestion")
        
        // Execute Fetch
        do {
            let response = try SavedQuestion.context.fetch(fetchRequest) as? [SavedQuestion]
            list = response?.reversed()
            
        } catch let error as NSError {
            os_log("Could not fetch. %@", error)
        }
        
    }
    
}
