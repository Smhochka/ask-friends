//
//  ASKQuestion.swift
//  
//
//  Created by Sonya Olson on 4/19/16.
//
//

import UIKit
import CoreData

enum SavedQuestionError: Error {
    case duplicateQuestion
    case databaseError
}

extension SavedQuestion : Question { }

// MARK: Static Functions
extension SavedQuestion {
    
    static var context:NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    static func createEntityFromQuestion(_ question:Question) -> SavedQuestion? {
        do {
            return try SavedQuestion.insertQuestion(question)
        } catch {
            return nil
        }
    }
    
    static func deleteEntityFromQuestion(_ question:Question) throws {
        if let savedQuestion = question as? SavedQuestion {
            try SavedQuestion.delete(savedQuestion)
        } else {
            let savedQuestion = try SavedQuestion.getDuplicates(question)
            for q in savedQuestion {
                try SavedQuestion.delete(q)
            }
        }
    }
    
    static func getDuplicates(_ question:Question) throws -> [SavedQuestion] {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "SavedQuestion")
        fetchRequest.predicate = NSPredicate(format: "id = %@", question.id! )
        
        // Execute Fetch
        let result = try SavedQuestion.context.fetch(fetchRequest) as! [SavedQuestion]

        return (result)
    }
    
    static func insertQuestion(_ question:Question) throws  -> SavedQuestion {
        guard try SavedQuestion.getDuplicates(question).count == 0 else { throw SavedQuestionError.duplicateQuestion }
        
        // Create New Saved Question Entity
        let entity = NSEntityDescription.insertNewObject(forEntityName: "SavedQuestion",
                                                         into: SavedQuestion.context)
        
        // Copy Question to Entity
        entity.setValue(question.title, forKeyPath: "title")
        entity.setValue(question.id, forKeyPath: "id")
        entity.setValue(question.link, forKeyPath: "link")
        entity.setValue(question.numComments, forKeyPath: "numComments")
        entity.setValue(true, forKeyPath: "saved")
        entity.setValue("favorite", forKeyPath: "source")
        
        // Save Context
        try SavedQuestion.context.save()
        
        return entity as! SavedQuestion
    }
    
    static func delete(_ question:SavedQuestion) throws {
        SavedQuestion.context.delete(question)
    }
    
}
